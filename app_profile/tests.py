from django.test import TestCase, Client
from .views import user_name
from django.urls import reverse

# Create your tests here.
class ProfileTest(TestCase):

    def test_app_profile_url_ok(self):
        response = Client().get(reverse('app_profile:index'))
        self.assertEqual(response.status_code, 200)

    def test_profile_contains_user_name(self):
        response = Client().get(reverse('app_profile:index'))
        response_content = response.content.decode('utf8')
        self.assertIn(user_name, response_content)