from django.shortcuts import render

user_name = 'Yes Mi'
birth_date = '31 February 1999'
user_gender = 'Unidentified'
user_description = 'Am I even a human being'
user_mail = 'yes@mi.com'
expertises = ['I bark', 'I run', 'I lick']

bio_dict = [{'subject' : 'Birthday', 'value' : '  ' + birth_date},\
{'subject' : 'Gender', 'value' : '  ' + user_gender},\
{'subject' : 'Description', 'value' : '  ' + user_description},\
{'subject' : 'Email', 'value' : '  ' + user_mail},\
            ]


# Create your views here.
def index(request):
    response = {'bio_dict' : bio_dict, 'expertises' : expertises, 'user_name':user_name}
    return render(request, 'app_profile/app_profile.html', response)