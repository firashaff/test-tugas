from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import Status_Form
from .models import Status
from app_profile.views import user_name


def index(request):
    response = {
        'profile_name':user_name,
        'status':Status.objects.order_by('-created_date'),
        'status_form':Status_Form
    }
    return render(request, 'app_status/status_list.html', response)


def add_status(request) :
    form = Status_Form(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        Status(status=request.POST['status']).save()

    return HttpResponseRedirect(reverse('app_status:index'))