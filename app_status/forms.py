from django import forms

class Status_Form(forms.Form):
    st_attrs = {
        'type': 'text',
        'rows': 1,
        'class': 'form-control',
        'placeholder':'What is on your mind?',
        'maxlength': 70,
        'size':5,
        'cols':30,
        'style':'resize:none',
    }

    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=st_attrs))
