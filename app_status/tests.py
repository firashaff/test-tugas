from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index,add_status
from .models import Status
from .forms import Status_Form
from django.urls import reverse

# Create your tests here.

class appStatusTest(TestCase):
    def test_app_status_url_is_exist(self):
        response = Client().get(reverse('app_status:index'))
        self.assertEqual(response.status_code,200)

    def test_app_status_using_status_list_func(self):
        found = resolve(reverse('app_status:index'))
        self.assertEqual(found.func,index)
    
    def test_model_can_create_new_status(self):
        Status.objects.create(status='Sedih banget nih')
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status,1)
    
    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'status':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'],['This field is required.'])

    def test_app_status_post_fail(self):
        response = Client().post(reverse('app_status:add_status'),{'status':''})
        self.assertEqual(response.status_code,302)
    
    def test_app_status_showing_all_status(self):
        test = 'PPW is Fun!'
        response_post = Client().post(reverse('app_status:add_status'), {'status' : test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get(reverse('app_status:index'))
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

