"""tugas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import app_friends.urls as friends
import app_profile.urls as profile
import app_statistics.urls as statistics
import app_status.urls as status
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^friends/', include(friends, namespace='app_friends')),
    url(r'^profile/', include(profile, namespace='app_profile')),
    url(r'^statistics/', include(statistics, namespace='app_statistics')),
    url(r'^status/', include(status, namespace='app_status')),
    url(r'^$', RedirectView.as_view(url='/profile/', permanent=True))
]