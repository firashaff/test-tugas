from django import forms

class Friends_Form(forms.Form):

    
    st_attrs = {
        'type': 'text',
        'rows': 1, 
        'class': 'form-control',
        'placeholder':'Masukan nama teman..',
        'maxlength': 70,
        'size':5,
        'cols':150,
        'style':'resize:none',
    }
    url_attrs = {
        'type': 'text',
        'rows': 1,
        'class': 'form-control',
        'placeholder':'Masukkan url teman..',
        'maxlength': 70,
        'size':5,
        'cols':150,
        'style':'resize:none',

    }
    error_messages ={
        'required' : 'Isi ya namanya',
        'invalid' : 'url tidak ada'

    }

    name = forms.CharField(label='Nama', required=True, widget=forms.Textarea(attrs=st_attrs))
    url = forms.URLField(label='URL  ',initial='http://', required=True, widget=forms.Textarea(attrs=url_attrs))



