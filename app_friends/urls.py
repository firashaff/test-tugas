from django.conf.urls import url
from .views import index,add_friends

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_friends/$', add_friends, name='add_friends'),
]
