from django.test import TestCase
from .models import Friends
from .forms import Friends_Form
from .views import index,add_friends, is_valid_url
from django.test import Client
from django.urls import resolve, reverse


class AddFriendsUnitTest(TestCase):

    def test_add_friends_url_is_exist(self):
        response = Client().get(reverse('app_friends:index'))
        self.assertEqual(response.status_code, 200)

    def test_add_friends_using_index_func(self):
        found = resolve(reverse('app_friends:index'))
        self.assertEqual(found.func, index)

    def test_model_can_create_new_friends(self):
         # Creating a new activity
        Friends.objects.create(name='Shafira F', url='gitlab.com')

            # Retrieving all available activity
        counting_all_available_friends = Friends.objects.all().count()
        self.assertEqual(counting_all_available_friends, 1)

    def test_form_validation_for_blank_items(self):
        form = Friends_Form(data={'name': '', 'url': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
        form.errors['name'],
        ["This field is required."])

    def test_add_friends_post_success_and_render_the_result(self):
        test = 'Anonymous'
        url = 'facebook.com'
        response_post = Client().post(reverse('app_friends:add_friends'), {'name': test, 'url': url})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get(reverse('app_friends:index'))
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
        self.assertIn(url, html_response)

    def test_add_friends_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post(reverse('app_friends:add_friends'), {'name': '', 'url': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get(reverse('app_friends:index'))
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)


    def test_add_friends_valid_url(self):
        valid_url = 'google.com'
        self.assertTrue(is_valid_url(valid_url))

    def test_add_friends_invalid_url(self):
        invalid_url = 'kljkljlkjiojifodsf'
        self.assertFalse(is_valid_url(invalid_url))

    def test_add_friends_send_invalid_url(self):
        test = 'Anonymous'
        url = 'fdsafdsa'
        self.assertFalse(is_valid_url(url))
        response_post = Client().post(reverse('app_friends:add_friends'), {'name': test, 'url': url})
        self.assertRedirects(response_post, reverse('app_friends:index')+ '?error=1')

    def test_add_friends_send_notfound_url(self):
        test = 'Anonymous'
        url = 'askdljalskd.herokuapp.com/askdlajsdas'
        self.assertFalse(is_valid_url(url))
        response_post = Client().post(reverse('app_friends:add_friends'), {'name': test, 'url': url})
        self.assertRedirects(response_post, reverse('app_friends:index') + '?error=1')
# Create your tests here.
