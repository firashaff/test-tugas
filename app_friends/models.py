from django.db import models
from django.utils import timezone


class Friends(models.Model):
    name = models.CharField(max_length=140)
    created_date = models.DateTimeField(default = timezone.now)
    url = models.CharField(max_length=140)


