from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Friends_Form
from .models import Friends
from app_profile.views import user_name
from django.urls import reverse
import httplib2
import re

# Create your views here.
response = {}
def index(request):
    response = {
        'profile_name':user_name,
        'friends':Friends.objects.order_by('-created_date'),
        'friends_form':Friends_Form,
        'is_error':(request.GET.get('error', None) is not None),
    }
    return render(request, 'app_friends/friends_list.html', response)


def add_friends(request) :
    form = Friends_Form(request.POST or None)
    if request.method == 'POST' and form.is_valid() :
        if is_valid_url(request.POST['url']):
            Friends(name=request.POST['name'],url=request.POST['url']).save()
        else:
            return HttpResponseRedirect(reverse('app_friends:index') + "?error=1")
    else:
        return HttpResponseRedirect(reverse('app_friends:index') + "?error=1")
    return HttpResponseRedirect(reverse('app_friends:index'))


def is_valid_url(url):
    if not (re.match("^http://", url) or re.match("^https://", url)):
        url = "http://" + url
    try:
        h = httplib2.Http()
        resp = h.request(url, "HEAD")
        return int(resp[0]['status']) < 400
    except:
        return False