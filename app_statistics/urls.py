from django.conf.urls import url
from .views import index, change_background

urlpatterns = [
    url(r'^cbg/', change_background, name='change_background'),
    url(r'^$', index, name='index'),
]
