from django.shortcuts import render
from app_friends.models import Friends
from app_status.models import Status
from app_profile.views import user_name
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
def index(request):
    response = {
        'profile_name':user_name,
        'post_count':Status.objects.count(),
        'friend_count':Friends.objects.count(),
    }

    if response['post_count'] > 0:
        response['latest_post'] = Status.objects.order_by('-created_date')[0]

    return render(request, 'app_statistics/statistics.html', response)

def change_background(request):
    if (request.POST):
        response = HttpResponseRedirect(reverse('app_statistics:change_background'))
        response.set_cookie('bg-color', request.POST['bg-color'])
        return response
    else:
        return render(request, 'app_statistics/change_background.html', None)