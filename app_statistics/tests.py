from django.test import TestCase, Client
from app_friends.models import Friends
from app_status.models import Status
from django.urls import reverse

# Create your tests here.
class AppStatisticsTest(TestCase):

    def test_statistics_app_url_ok(self):
        response = Client().get(reverse('app_statistics:index'))
        self.assertEqual(response.status_code, 200)

    def test_statistics_app_friend_count(self):
        Friends.objects.create(name = "Musibah Smith", url = "youareanidiot.org")
        Friends.objects.create(name="Teretetet", url="turututt.org")
        response = Client().get(reverse('app_statistics:index'))
        response_content = response.content.decode('utf8')
        self.assertIn('2', response_content)

    def test_statistics_app_status_count(self):
        Status.objects.create(status = "Saya ganteng")
        response = Client().get(reverse('app_statistics:index'))
        response_content = response.content.decode('utf8')
        self.assertIn('1', response_content)

    def test_statistics_app_latest_status(self):
        Status.objects.create(status="Saya ganteng 1")
        Status.objects.create(status="Saya ganteng 2")
        Status.objects.create(status="Saya ganteng 3")
        response = Client().get(reverse('app_statistics:index'))
        response_content = response.content.decode('utf8')
        self.assertIn("Saya ganteng 3", response_content)

    def test_statistics_app_change_background_url_ok(self):
        response = Client().get(reverse('app_statistics:change_background'))
        self.assertEqual(response.status_code, 200)

    def test_statistics_app_change_background_post(self):
        response_post = Client().post(reverse('app_statistics:change_background'), {'bg-color': '#FFFFFF'})
        self.assertRedirects(response_post, reverse('app_statistics:change_background'))